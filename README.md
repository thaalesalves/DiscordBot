# Discord Bot (TESBR e UESPWiki)

Este bot foi feito para gerenciar o Discord da The Elder Scrolls Brasil. Ele integra suas funções com a página e site da UESPWiki.
- Gerenciamento do servidor (avisos, chutes e banimentos)
- Busca de artigos da UESPWiki na categoria de lore
- Busca de posts das páginas UESPWiki e TESBR no Facebook
- Busca de insights das páginas da leitura mais facilitada

## Este bot funciona para qualquer servidor?
Note que este não é um bot, e sim apenas um programa que gera funções para um. O bot é criado na página de desenvolvedores do Discord, e o token gerado lá pode ser inserido neste código para ele controlar outros bots.

## Ele invade funções no Facebook?
Não. Temos um app no Facebook criado especialmente para essas páginas, e ele tem permissões específicas de acessar somente dados de posts e insights.

## Como ele integra?
Por enquanto estamos apenas em fase de testes com a integração com Facebook. Vamos tentar integrar por webhooks ou direto pela aplicação, o que funcionar melhor.