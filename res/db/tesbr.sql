/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  thaal
 * Created: Oct 1, 2017
 */

DROP TABLE command;
DROP TABLE image;
DROP TABLE gartic;

-- Tabela de Imagens
CREATE TABLE image (
    id SERIAL PRIMARY KEY,
    cmd VARCHAR NOT NULL,    
    url VARCHAR NOT NULL,
    msg VARCHAR NOT NULL
);
INSERT INTO image VALUES(DEFAULT, 'matilda', 'https://scontent.fgru6-1.fna.fbcdn.net/v/t1.0-9/22228333_281210405729673_1267264151560548505_n.jpg?oh=23631d9a97aab07eb396dc7caadfa3dd&oe=5A7E3082', 'I see heads rolling...');

-- Tabela de Comandos
CREATE TABLE command (
    id SERIAL PRIMARY KEY,
    cmd VARCHAR NOT NULL,
    msg VARCHAR NOT NULL
);
INSERT INTO command VALUES(DEFAULT, 'eso', 'Você quis dizer TESO?');
INSERT INTO command VALUES(DEFAULT, 'moonandstar', 'By the One-Clan-Under-Moon-and-Star, I hereby name thee the Asshole of the PSJJJ.');
INSERT INTO command VALUES(DEFAULT, 'talk', 'Need something?');
INSERT INTO command VALUES(DEFAULT, 'ysgramor', 'Ysgramor matou foi pouco');
INSERT INTO command VALUES(DEFAULT, 'teamo', 'Eu não.');
INSERT INTO command VALUES(DEFAULT, 'obrigado', 'Não, obrigado você');
INSERT INTO command VALUES(DEFAULT, 'obrigadovc', 'Não, que isso, obrigado você!');
INSERT INTO command VALUES(DEFAULT, 'vehk', 'DID YOU JUST ASSUME MY GENDER, FETCHER?!?!?!?!?!');
INSERT INTO command VALUES(DEFAULT, 'skyrim', 'Compre Skyrim para SNES e Gameboy Advance! Disponível em 11/11/17!');

-- Tabela de Gartic
CREATE TABLE gartic (
    id SERIAL PRIMARY KEY,
    tema VARCHAR NOT NULL,
    url VARCHAR NOT NULL,
    dificuldade VARCHAR NOT NULL,
    senha VARCHAR NOT NULL,
    pontuacao VARCHAR NOT NULL
);
INSERT INTO gartic VALUES(DEFAULT, 'Lore', 'http://gartic.me/0FJXg', 'muito difícil', 'teslore', '200');
INSERT INTO gartic VALUES(DEFAULT, 'Skyrim', 'http://gartic.me/0D1qU', 'normal', 'tesbr', '200');
INSERT INTO gartic VALUES(DEFAULT, 'Geral', 'http://gartic.me/0D1pR', 'normal', 'tesbr', '200');
INSERT INTO gartic VALUES(DEFAULT, 'Objetos', 'http://gartic.me/0FStD', 'normal', 'tesbr', '200');