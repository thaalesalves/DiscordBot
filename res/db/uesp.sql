/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  thaal
 * Created: Oct 1, 2017
 */

DROP TABLE command;
DROP TABLE image;

-- Tabela de Imagens
CREATE TABLE image (
    id SERIAL PRIMARY KEY,
    cmd VARCHAR NOT NULL,    
    url VARCHAR NOT NULL,
    msg VARCHAR NOT NULL
);

INSERT INTO image VALUES(DEFAULT, 'lorebreaker', 'https://i.imgur.com/KvW3e0q.jpg', 'YOU JUST GOT LOR''D');
INSERT INTO image VALUES(DEFAULT, 'muatra', 'https://i.imgur.com/keWXMCL.png', 'AE MUATRA CE ALTADOON');
INSERT INTO image VALUES(DEFAULT, 'mk', 'http://lagbt.wiwiland.net/images/thumb/b/b2/Michael_Kirkbride.jpg/300px-Michael_Kirkbride.jpg', 'MK AE C0DA AE TESLORE');
INSERT INTO image VALUES(DEFAULT, 'todd', 'http://i0.kym-cdn.com/photos/images/original/001/264/884/197.png', 'IF YOU HAVEN''T BOUGHT SKYRYM YET, GO DO IT');
INSERT INTO image VALUES(DEFAULT, 'chim', 'http://68.media.tumblr.com/63db50844870018fa5690d8a530b4ec5/tumblr_inline_nn6zydWlnS1tsdgjg_500.jpg', 'AE CHIM CE ALTADOON');
INSERT INTO image VALUES(DEFAULT, 'delicia', 'https://i.warosu.org/data/tg/img/0320/61/1399829602127.jpg', 'CHIMaravilha');
INSERT INTO image VALUES(DEFAULT, 'scum', 'https://i.ytimg.com/vi/rWFPw8Lt1bk/hqdefault.jpg', 'YOUR STOLEN GOODS ARE NOW FORFEIT');
INSERT INTO image VALUES(DEFAULT, 'cheese', 'https://orig00.deviantart.net/357e/f/2012/334/c/1/c19138e575fd96152189e7d0087e00df-d5mluv5.jpg', 'CHEESE FOR EVERYONE');
INSERT INTO image VALUES(DEFAULT, 'yagrum', 'https://scontent.fgru6-1.fna.fbcdn.net/v/t1.0-9/22195811_1543856372329403_5197833081553129274_n.jpg?oh=5c52acabce6cccf7d86ebb583e0ff68d&oe=5A401468', 'O ┌LTIMO DWEMER VIVO, DJ KHAL... DIGO, YAGRUM BAGARN');

-- Tabela de Comandos
CREATE TABLE command (
    id SERIAL PRIMARY KEY,
    cmd VARCHAR NOT NULL,
    msg VARCHAR NOT NULL
);

