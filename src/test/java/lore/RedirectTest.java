/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lore;

import es.thalesalv.discordbotjda.util.JSON;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class RedirectTest {

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        PrintStream out = new PrintStream(System.out);

        out.print("Digite um tópico: ");
        String content = "/lore " + in.nextLine();
        String title;
        String op = "/";
        String cmd = content.split("\\s+")[0];
        String param = content.replace(cmd + " ", "");

        out.println("\nComando: " + cmd + "\n");
        out.println("\nParâmetro: " + param + "\n");

        JSONObject obj = getLoreJson(param);
        String pageid = getLoreJsonPageId(param);
        String art = "";

        if (Integer.parseInt(pageid) < 1) {
            out.println("Não foi possível encontrar lore sobre " + WordUtils.capitalize(param));
        } else {
            obj = obj.getJSONObject(pageid);
            title = obj.getString("title");
            JSONArray obja = obj.getJSONArray("revisions");
            art = obja.toString();
            art = art
                    .replace("text\"}]", "text\"}")
                    .replace("[{\"conte", "{\"conte");
            obj = new JSONObject(art);
            String cont = obj.getString("conteudo");
            Pattern p = null;
            Matcher m = null;
            cont = cont.replace("_", " ");

            if (cont.contains("#REDIRECT")) {
                System.out.println("Página com redirecionamento: " + title + "\n");
                p = Pattern.compile("REDIRECT\\[\\[Lore:(.*?)\\]\\]");
                m = p.matcher(cont);
                while (m.find()) {
                    cont = m.group(1);
                }

                p = Pattern.compile("REDIRECT \\[\\[Lore:(.*?)\\]\\]");
                m = p.matcher(cont);
                while (m.find()) {
                    cont = m.group(1);
                }

                obj = getLoreJson(cont.replace("_", " "));
                pageid = getLoreJsonPageId(cont.replace("_", " "));
                obj = obj.getJSONObject(pageid);
                obja = obj.getJSONArray("revisions");
                art = obja.toString();
                art = art.replace("text\"}]", "text\"}").replace("[{\"conte", "{\"conte");
                obj = new JSONObject(art);
                cont = obj.getString("conteudo");

                cont = cont.replace("_", " ");
                p = Pattern.compile("\\{\\{Discord\\|(.*?)\\}\\}");
                m = p.matcher(cont);
                while (m.find()) {
                    cont = m.group(1);
                }

                out.println(cont + "\n\nLeia mais em http://pt.uesp.net/wiki/" + title.replace(" ", "_"));
            } else {
                out.println("O artigo " + title + " ainda não foi adaptado para o Discord. Entre em contato com a administração da UESPWiki.\n\nLeia mais em http://pt.uesp.net/wiki/" + title.replace(" ", "_"));
            }
        }
    }

    public static JSONObject getLoreJson(String param) throws IOException {
        param = WordUtils.capitalize(param);
        String page = "Lore:" + param
                .replace(" ", "_")
                .replace("De", "de")
                .replace("Da", "da")
                .replace("Do", "do")
                .replace("Na", "Na")
                .replace("No", "no")
                .replace("Para", "para")
                .replace("deshaan", "Deshaan")
                .replace("vvardenfell", "Vvardenfell");

        String art = JSON.toJson("http://pt.uesp.net/w/api.php?action=query&titles=" + page + "&prop=revisions&rvprop=content&format=json");
        art = art.replace("\"*\"", "\"conteudo\"");
        JSONObject obj = new JSONObject(art);
        obj = obj.getJSONObject("query");
        obj = obj.getJSONObject("pages");

        System.out.println("\nEstou retornando o objeto JSON (no método getLoreJason(String)): " + page);

        return obj;
    }

    public static String getLoreJsonPageId(String param) throws IOException {
        param = WordUtils.capitalize(param);
        String page = "Lore:" + param
                .replace(" ", "_")
                .replace("De", "de")
                .replace("Da", "da")
                .replace("Do", "do")
                .replace("Na", "Na")
                .replace("No", "no")
                .replace("Para", "para")
                .replace("deshaan", "Deshaan")
                .replace("vvardenfell", "Vvardenfell");

        String pageid = "";
        String art = JSON.toJson("http://pt.uesp.net/w/api.php?action=query&titles=" + page + "&prop=revisions&rvprop=content&format=json");
        art = art.replace("\"*\"", "\"conteudo\"");
        JSONObject obj = new JSONObject(art);
        obj = obj.getJSONObject("query");
        obj = obj.getJSONObject("pages");

        for (Object key : obj.keySet()) {
            pageid = key.toString();
        }

        System.out.println("\nEstou retornando o objeto JSON (no método getLoreJsonPageId(String)): " + page);

        return pageid;
    }
}
