/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facebook;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import es.thalesalv.discordbotjda.util.IO;
import es.thalesalv.discordbotjda.util.JSON;
import es.thalesalv.discordbotjda.util.Logger;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONObject;

public class PostPageTest {

    @lombok.Getter
    @lombok.Setter
    class Response {

        Map<String, Data> data;
    }

    @lombok.Getter
    @lombok.Setter
    class Data {

        String id;
        String pictureUrl;
        User adminCreator;
    }

    @lombok.Getter
    @lombok.Setter
    class User {

        String id;
        String name;
    }

    public static void main(String[] args) {
        JsonParser parser = new JsonParser();
        
        try {
            String json = JSON.toJson("https://graph.facebook.com/467115659967426/feed?access_token=EAACEdEose0cBADUVHau38cRz4daVVNHjGPX8FAGqICAZBNkD0ZBNSTtjZBcl1U4rCqZB3P1MfDF1gTAUwgc1s5Av2nrPvpSKh3pTYeEiYwaikXDZCysuZAhWOJWWL0cEjQbjZBZCzjPKRl4FbOAMMggc10IHViH1xUUnulvZBJPvBM435PJLMFYKbprnh6rdWSf8ZD&fields=id,admin_creator,description,picture");

            Pattern p = Pattern.compile("\\{\"data\":\\[(.*?),\\{\"id\"");
            Matcher m = p.matcher(json);
            while (m.find()) {
                json = m.group(1).replace("\\/", "/");
            }

            JsonElement jsonElement = parser.parse(json);
            IO.writeln("JSON original: " + jsonElement.toString());
            
            /*JSONObject obj = new JSONObject();
            obj = obj.getJSONObject(jsonElement.toString());
            obj = obj.getJSONObject("id");
            IO.writeln("ID: " + obj.toString());*/
        } catch (Exception e) {
            Logger.logSevere(e, PostPageTest.class);
        }
    }
}
