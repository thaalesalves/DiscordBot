/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.cmd;

import es.thalesalv.discordbotjda.model.Lore;
import es.thalesalv.discordbotjda.util.Logger;
import es.thalesalv.discordbotjda.util.MediaWiki;
import net.sourceforge.jwbf.core.contentRep.Article;
import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;

public class LoreCommand {

    public Lore lore(String param) {
        param = MediaWiki.parsePageTitle(param);
        MediaWikiBot wikiBot = new MediaWikiBot("http://pt.uesp.net/w/");
        Article a = wikiBot.getArticle("Lore:" + param);
        Lore l = new Lore();

        try {
            if (a.getText().contains("{{Discord|")) {
                l.setTitle(a.getTitle().replace("_", " "));
                l.setContent(MediaWiki.getBetweenDiscord(a.getText()));
                l.setUrl("http://pt.uesp.net/wiki/Lore:" + MediaWiki.parsePageTitle(param));
                return l;
            } else if (a.getText().contains("#REDIRECT")) {
                param = MediaWiki.getBetweenRedirect(a.getText());
                l.setTitle(a.getTitle().replace("_", " "));
                l.setUrl("http://pt.uesp.net/wiki/Lore:" + MediaWiki.parsePageTitle(param));
                a = wikiBot.getArticle("Lore:" + param);
                l.setContent(MediaWiki.getBetweenDiscord(a.getText()));
                return l;
            }
        } catch (Exception e) {
            Logger.logSevere(e, LoreCommand.class);
        }

        return null;
    }
}
