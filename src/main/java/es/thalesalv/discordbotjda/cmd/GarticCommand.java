/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.cmd;

import es.thalesalv.discordbotjda.dao.GarticDAO;
import es.thalesalv.discordbotjda.model.Gartic;
import es.thalesalv.discordbotjda.util.Logger;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.core.EmbedBuilder;

public class GarticCommand {

    GarticDAO gdao = new GarticDAO();

    private Gartic getGartic(Gartic g) {
        try {
            g = gdao.select(g);
        } catch (Exception e) {
            Logger.logSevere(e, GarticCommand.class);
        }

        return g;
    }

    private List<Gartic> getGartic() {
        List<Gartic> gs = new ArrayList<Gartic>();

        try {
            gs = gdao.select();
        } catch (Exception e) {
            Logger.logSevere(e, GarticCommand.class);
        }

        return gs;
    }

    public EmbedBuilder getGartic(String param) {
        Gartic g = new Gartic();
        g.setTema(param);
        g = getGartic(g);

        EmbedBuilder builder = new EmbedBuilder();
        builder.setColor(Color.BLUE);
        builder.setTitle("Sala de Gartic: " + g.getTema());
        builder.setDescription("**Senha:** " + g.getSenha() + "\n**Dificuldade:** " + g.getDificuldade() + "\n**Link:** " + g.getUrl() + "\n**Termina em:** " + g.getPontuacao() + " pontos");
        builder.setImage("https://scontent.fgru6-1.fna.fbcdn.net/v/t1.0-9/21231186_849420248557198_5752852703666970969_n.jpg?oh=bd364e10f0a257c752615bbdb21879c1&oe=5A860795");
        builder.setFooter("The Elder Scrolls Brasil. A maior comunidade de Elder Scrolls do Brasil!", "https://scontent.fgru6-1.fna.fbcdn.net/v/t1.0-9/20841006_1658564530822527_421065287652179817_n.png?oh=7970b7d4f8ad7c07ed1ba2c3f9521514&oe=5A494BE7");

        return builder;
    }

    public EmbedBuilder getGarticMenu() {
        List<Gartic> gs = getGartic();
        String cont = "";

        EmbedBuilder builder = new EmbedBuilder();
        builder.setColor(Color.BLUE);

        for (Gartic i : gs) {
            cont = cont + " \n **SALA: " + i.getTema().toUpperCase() + "** \n  **Senha:** " + i.getSenha() + " \n  **Dificuldade:** " + i.getDificuldade() + " \n  **Link:** " + i.getUrl() + " \n  **Termina em:** " + i.getPontuacao() + " pontos";
        }

        builder.setTitle("Salas de Gartic da Elder Scrolls Brasil");
        builder.setDescription("Temos algumas salas de Gartic, cada uma com tema diferente. Todas elas terminam em 200 pontos, e suportam até 50 pessoas.\n" + cont);
        builder.setImage("https://scontent.fgru6-1.fna.fbcdn.net/v/t1.0-9/21231186_849420248557198_5752852703666970969_n.jpg?oh=bd364e10f0a257c752615bbdb21879c1&oe=5A860795");
        builder.setFooter("The Elder Scrolls Brasil. A maior comunidade de Elder Scrolls do Brasil!", "https://scontent.fgru6-1.fna.fbcdn.net/v/t1.0-9/20841006_1658564530822527_421065287652179817_n.png?oh=7970b7d4f8ad7c07ed1ba2c3f9521514&oe=5A494BE7");

        return builder;
    }
}
