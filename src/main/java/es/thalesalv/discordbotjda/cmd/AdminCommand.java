/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.cmd;

import es.thalesalv.discordbotjda.model.Command;
import es.thalesalv.discordbotjda.model.Image;
import es.thalesalv.discordbotjda.util.Logger;
import java.awt.Color;
import java.util.List;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.exceptions.ErrorResponseException;
import net.dv8tion.jda.core.requests.Request;
import net.dv8tion.jda.core.requests.Response;
import net.dv8tion.jda.core.requests.Route;
import net.dv8tion.jda.core.requests.restaction.AuditableRestAction;
import net.dv8tion.jda.core.utils.MiscUtil;

public class AdminCommand {

    public boolean checkPermission(Member member, Permission perm) {
        if (member.hasPermission(perm)) {
            return true;
        }
        return false;
    }

    public EmbedBuilder ban(Member admin, User user, Guild guild, String motivo) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.setColor(Color.BLUE);
        try {
            if (checkPermission(admin, Permission.BAN_MEMBERS)) {
                Route.CompiledRoute route = Route.Guilds.BAN.compile(guild.getId(), user.getId());
                if (motivo != null && !motivo.isEmpty()) {
                    route = route.withQueryParams("param", MiscUtil.encodeUTF8(motivo));
                }

                AuditableRestAction ars = new AuditableRestAction<Void>(guild.getJDA(), route) {
                    @Override
                    protected void handleResponse(Response response, Request<Void> request) {
                        if (response.isOk()) {
                            request.onSuccess(null);
                        } else {
                            request.onFailure(response);
                        }
                    }
                };
                ars.complete();
                builder.setTitle("Atenção, " + user.getName() + "!");
                builder.setDescription(user.getAsMention() + " acaba de ser banido do servidor por: " + motivo + ". Comportem-se!");
            } else {
                builder.setTitle("Erro!");
                builder.setDescription("Opa! Parece que você não tem permissão para banir membros!");
            }
        } catch (ErrorResponseException e) {
            builder.setTitle("Erro!");
            builder.setDescription("Opa! Parece que você não tem permissão para banir membros!");
            return builder;
        } catch (Exception e) {
            Logger.logSevere(e, AdminCommand.class);
        }
        return builder;
    }

    public EmbedBuilder kick(Member admin, User user, Guild guild, String motivo) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.setColor(Color.BLUE);
        try {
            if (checkPermission(admin, Permission.KICK_MEMBERS)) {
                Route.CompiledRoute route = Route.Guilds.KICK_MEMBER.compile(guild.getId(), user.getId());
                if (motivo != null && !motivo.isEmpty()) {
                    route = route.withQueryParams("param", MiscUtil.encodeUTF8(motivo));
                }

                AuditableRestAction ars = new AuditableRestAction<Void>(guild.getJDA(), route) {
                    @Override
                    protected void handleResponse(Response response, Request<Void> request) {
                        if (response.isOk()) {
                            request.onSuccess(null);
                        } else {
                            request.onFailure(response);
                        }
                    }
                };

                ars.complete();
                builder.setTitle("Atenção, " + user.getName() + "!");
                builder.setDescription(user.getAsMention() + " acaba de ser chutado do servidor por: " + motivo + ". Comportem-se!");
            } else {
                builder.setTitle("Erro!");
                builder.setDescription("Opa! Parece que você não tem permissão para chutar membros!");
            }
        } catch (ErrorResponseException e) {
            builder.setTitle("Erro!");
            builder.setDescription("Opa! Parece que você não tem permissão para chutar membros!");
            return builder;
        } catch (Exception e) {
            Logger.logSevere(e, AdminCommand.class);
        }
        return builder;
    }

    public EmbedBuilder warn(User user, String motivo) {
        EmbedBuilder builder = new EmbedBuilder();
        try {
            builder.setColor(Color.BLUE);
            builder.setTitle("Atenção, " + user.getName() + "!");
            builder.setDescription("Você está sendo advertido por:" + motivo + ". Se comporte, " + user.getAsMention() + "!");
        } catch (Exception e) {
            Logger.logSevere(e, AdminCommand.class);
        }
        return builder;
    }

    public EmbedBuilder helpTesbr(String cmd) {
        EmbedBuilder builder = new EmbedBuilder();
        try {
            ImageCommand ic = new ImageCommand();
            CommandCommand cc = new CommandCommand();
            List<Image> is = ic.selecionarTesbr();
            List<Command> cs = cc.selectTesbr();

            String img = "";
            cmd = "";
            String imgMenu = "";
            String cmdMenu = "";

            for (Image i : is) {
                img = img + "- " + i.getCmd() + "\n";
                imgMenu = "\n\n== IMAGENS ==\n";
            }

            for (Command i : cs) {
                cmd = cmd + "- " + i.getCmd() + "\n";
                cmdMenu = "\n== COMANDOS ==\n";
            }
            builder.setColor(Color.BLUE);
            builder.setTitle("Comandos do bot da TESBR");
            builder.setDescription("== COMANDOS ADMINISTRATIVOS ==\n"
                    + "- **chute** *mention* *motivo*: chuta o usuário especificado do servidor\n"
                    + "- **ban** *mention* *motivo*: bane o usuário especificado do servidor\n"
                    + "\n== COMANDOS PRINCIPAIS ==\n"
                    + "- **gartic**: mostra os dados de todas as salas de Gartic\n"
                    + "- **gartic** *tema*: mostra os dados da sala de Gartic informada\n"
                    + "- **pvt** *mention* *mensagem*: envia uma mensagem privada ao usuário mencionado\n" + imgMenu + img + cmdMenu + cmd);
        } catch (Exception e) {
            Logger.logSevere(e, AdminCommand.class);
        }
        return builder;
    }

    public EmbedBuilder helpUesp() {
        EmbedBuilder builder = new EmbedBuilder();
        try {
            ImageCommand ic = new ImageCommand();
            CommandCommand cc = new CommandCommand();
            List<Image> is = ic.selecionarUesp();
            List<Command> cs = cc.selectUesp();

            String img = "";
            String cmd = "";
            String imgMenu = "";
            String cmdMenu = "";

            for (Image i : is) {
                img = img + "- " + i.getCmd() + "\n";
                imgMenu = "\n\n== IMAGENS ==\n";
            }

            for (Command i : cs) {
                cmd = cmd + "- " + i.getCmd() + "\n";
                cmdMenu = "\n== COMANDOS ==\n";
            }

            builder.setColor(Color.RED);
            builder.setFooter("The UESPWiki. Your source for Elder Scrolls lore since 1995.", "https://i.imgur.com/4S08E2M.png");
            builder.setTitle("Comandos do bot da UESPWiki");
            builder.setDescription("== COMANDOS PRINCIPAIS ==\n"
                    //+ "- **vcsabia**: exibe o link de um post de curiosidade na página da UESPWiki\n"
                    + "- **lore** *param*: busca na UESPWiki a página da lore do termo que você pediu" + imgMenu + img + cmdMenu + cmd);
        } catch (Exception e) {
            Logger.logSevere(e, AdminCommand.class);
        }
        return builder;
    }

    public EmbedBuilder announce(String param) {
        EmbedBuilder builder = new EmbedBuilder();
        try {
            builder.setTitle("Anúncio da Staff!");
            builder.setDescription("Atenção, @everyone!\n" + param);
        } catch (Exception e) {
            Logger.logSevere(e, AdminCommand.class);
        }
        return builder;
    }
}
