/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.cmd;

import es.thalesalv.discordbotjda.cmd.ImageCommand;
import es.thalesalv.discordbotjda.dao.CommandDAO;
import es.thalesalv.discordbotjda.model.Command;
import es.thalesalv.discordbotjda.model.Image;
import es.thalesalv.discordbotjda.util.Logger;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.core.EmbedBuilder;

public class CommandCommand {

    CommandDAO dao = new CommandDAO();

    public Command selecionarComandoUesp(String cmd, String op) {
        Command c = new Command();

        try {
            List<Command> cs = dao.selectUesp();

            for (Command i : cs) {
                if (cmd.equalsIgnoreCase(op + i.getCmd())) {
                    c = i;
                }
            }

            if (c.getMsg() == null) {
                return null;
            } else {
                return c;
            }
        } catch (Exception e) {
            Logger.logSevere(e, ImageCommand.class);
        }

        return null;
    }
    
    public EmbedBuilder selecionarUesp(String cmd, String op) {
        EmbedBuilder builder = new EmbedBuilder();

        try {
            List<Command> cs = dao.selectUesp();

            for (Command c : cs) {
                if (cmd.equalsIgnoreCase(op + c.getCmd())) {

                    builder.setDescription(c.getMsg());
                    builder.setColor(Color.RED);
                    builder.setFooter("The UESPWiki. Your source for Elder Scrolls lore since 1995.", "https://i.imgur.com/4S08E2M.png");
                }
            }
        } catch (Exception e) {
            Logger.logSevere(e, ImageCommand.class);
        }

        return builder;
    }

    public List<Command> selectUesp() {
        List<Command> cs = new ArrayList<Command>();

        try {
            cs = dao.selectUesp();
        } catch (Exception e) {
            Logger.logSevere(e, CommandCommand.class);
        }
        return cs;
    }

    public Command selectUesp(Command c) {
        try {
            c = dao.selectUesp(c);
        } catch (Exception e) {
            Logger.logSevere(e, CommandCommand.class);
        }
        return c;
    }

    /* Elder Scrolls Brasil */
    public Command selecionarComandoTesbr(String cmd, String op) {
        Command c = new Command();

        try {
            List<Command> cs = dao.selectTesbr();

            for (Command i : cs) {
                if (cmd.equalsIgnoreCase(op + i.getCmd())) {
                    c = i;
                }
            }

            if (c.getMsg() == null) {
                return null;
            } else {
                return c;
            }
        } catch (Exception e) {
            Logger.logSevere(e, ImageCommand.class);
        }

        return null;
    }

    public EmbedBuilder selecionarTesbr(String cmd, String op) {
        EmbedBuilder builder = new EmbedBuilder();

        try {
            List<Command> cs = dao.selectTesbr();

            for (Command c : cs) {
                if (cmd.equalsIgnoreCase(op + c.getCmd())) {

                    builder.setDescription(c.getMsg());
                    builder.setColor(Color.BLUE);
                    builder.setFooter("The Elder Scrolls Brasil. A maior comunidade de Elder Scrolls do Brasil!", "https://scontent.fgru6-1.fna.fbcdn.net/v/t1.0-9/20841006_1658564530822527_421065287652179817_n.png?oh=7970b7d4f8ad7c07ed1ba2c3f9521514&oe=5A494BE7");
                }
            }
        } catch (Exception e) {
            Logger.logSevere(e, ImageCommand.class);
        }

        return builder;
    }

    public List<Command> selectTesbr() {
        List<Command> cs = new ArrayList<Command>();

        try {
            cs = dao.selectTesbr();
        } catch (Exception e) {
            Logger.logSevere(e, CommandCommand.class
            );
        }
        return cs;
    }

    public Command selectTesbr(Command c) {
        try {
            c = dao.selectTesbr(c);
        } catch (Exception e) {
            Logger.logSevere(e, CommandCommand.class);
        }
        return c;
    }
}
