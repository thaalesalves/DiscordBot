/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.dao;

import es.thalesalv.discordbotjda.model.Image;
import java.sql.SQLException;
import es.thalesalv.discordbotjda.util.Logger;
import es.thalesalv.discordbotjda.util.DatabaseConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ImageDAO {

    public void insertUesp(Image img) throws SQLException, ClassNotFoundException {
        try (Connection con = DatabaseConnection.getUespConnection()) {
            PreparedStatement pstmt = con.prepareStatement("INSERT INTO image VALUES(DEFAULT, ?, ?, ?)");
            pstmt.setString(1, img.getCmd());
            pstmt.setString(2, img.getUrl());
            pstmt.setString(3, img.getMsg());
            pstmt.executeUpdate();
            con.close();
        } catch (Exception e) {
            Logger.logSevere(e, ImageDAO.class);
        }
    }

    public Image selectUesp(Image img) throws SQLException, ClassNotFoundException {
        try (Connection con = DatabaseConnection.getUespConnection()) {
            PreparedStatement pstmt = con.prepareStatement("SELECT * FROM image WHERE comando = ?");
            pstmt.setString(1, img.getCmd());
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                img.setId(rs.getInt("id"));
                img.setUrl(rs.getString("link"));
                img.setMsg(rs.getString("mensagem"));
            }

            con.close();
        } catch (Exception e) {
            Logger.logSevere(e, ImageDAO.class);
        }

        return img;
    }

    public List<Image> selectUesp() throws SQLException, ClassNotFoundException {
        List<Image> is = new ArrayList<Image>();

        try (Connection con = DatabaseConnection.getUespConnection()) {
            PreparedStatement pstmt = con.prepareStatement("SELECT * FROM image");
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Image i = new Image();
                i.setCmd(rs.getString("cmd"));
                i.setId(rs.getInt("id"));
                i.setUrl(rs.getString("url").trim());
                i.setMsg(rs.getString("msg").trim());
                is.add(i);
            }

            con.close();
        } catch (Exception e) {
            Logger.logSevere(e, ImageDAO.class);
        }

        return is;
    }

    /* Elder Scrolls Brasil */
    public void insertTesbr(Image img) throws SQLException, ClassNotFoundException {
        try (Connection con = DatabaseConnection.getTesbrConnection()) {
            PreparedStatement pstmt = con.prepareStatement("INSERT INTO image VALUES(DEFAULT, ?, ?, ?)");
            pstmt.setString(1, img.getCmd());
            pstmt.setString(2, img.getUrl());
            pstmt.setString(3, img.getMsg());
            pstmt.executeUpdate();
            con.close();
        } catch (Exception e) {
            Logger.logSevere(e, ImageDAO.class);
        }
    }

    public Image selectTesbr(Image img) throws SQLException, ClassNotFoundException {
        try (Connection con = DatabaseConnection.getTesbrConnection()) {
            PreparedStatement pstmt = con.prepareStatement("SELECT * FROM image WHERE cmd = ?");
            pstmt.setString(1, img.getCmd());
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                img.setId(rs.getInt("id"));
                img.setUrl(rs.getString("url"));
                img.setMsg(rs.getString("msg"));
            }

            con.close();
        } catch (Exception e) {
            Logger.logSevere(e, ImageDAO.class);
        }

        return img;
    }

    public List<Image> selectTesbr() throws SQLException, ClassNotFoundException {
        List<Image> is = new ArrayList<Image>();

        try (Connection con = DatabaseConnection.getTesbrConnection()) {
            PreparedStatement pstmt = con.prepareStatement("SELECT * FROM image");
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Image i = new Image();
                i.setCmd(rs.getString("cmd"));
                i.setId(rs.getInt("id"));
                i.setUrl(rs.getString("url").trim());
                i.setMsg(rs.getString("msg").trim());
                is.add(i);
            }

            con.close();
        } catch (Exception e) {
            Logger.logSevere(e, ImageDAO.class);
        }

        return is;
    }
}
