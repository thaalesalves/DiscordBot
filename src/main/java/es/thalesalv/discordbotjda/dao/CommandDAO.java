/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.dao;

import es.thalesalv.discordbotjda.model.Command;
import es.thalesalv.discordbotjda.util.DatabaseConnection;
import es.thalesalv.discordbotjda.util.Logger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CommandDAO {

    /* UESPWiki */
    public void insertUesp(Command c) throws SQLException, ClassNotFoundException {
        try (Connection con = DatabaseConnection.getUespConnection()) {
            PreparedStatement pstmt = con.prepareStatement("INSERT INTO command VALUES(DEFAULT, ?, ?)");
            pstmt.setString(1, c.getCmd());
            pstmt.setString(2, c.getMsg());
            pstmt.executeUpdate();
            con.close();
        }
    }
    
    public List<Command> selectUesp() throws SQLException, ClassNotFoundException {
        List<Command> cs = new ArrayList<Command>();

        try (Connection con = DatabaseConnection.getUespConnection()) {
            PreparedStatement pstmt = con.prepareStatement("SELECT * FROM command");
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Command c = new Command();
                c.setCmd((rs.getString("cmd")));
                c.setId(rs.getInt("id"));
                c.setMsg(rs.getString("msg"));
                cs.add(c);
            }

            con.close();
        }

        return cs;
    }

    public Command selectUesp(Command c) throws SQLException, ClassNotFoundException {
        try (Connection con = DatabaseConnection.getUespConnection()) {
            PreparedStatement pstmt = con.prepareStatement("SELECT * FROM command WHERE comando = ?");
            pstmt.setString(1, c.getCmd());
            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                c.setCmd((rs.getString("cmd")));
                c.setId(rs.getInt("id"));
                c.setMsg(rs.getString("msg"));
            }

            con.close();
        } catch (Exception e) {
            Logger.logSevere(e, CommandDAO.class);
        }
        return c;
    }

    /* Elder Scrolls Brasil */
    public void insertTesbr(Command c) throws SQLException, ClassNotFoundException {
        try (Connection con = DatabaseConnection.getTesbrConnection()) {
            PreparedStatement pstmt = con.prepareStatement("INSERT INTO command VALUES(DEFAULT, ?, ?)");
            pstmt.setString(1, c.getCmd());
            pstmt.setString(2, c.getMsg());
            pstmt.executeUpdate();
            con.close();
        }
    }
    
    public List<Command> selectTesbr() throws SQLException, ClassNotFoundException {
        List<Command> cs = new ArrayList<Command>();

        try (Connection con = DatabaseConnection.getTesbrConnection()) {
            PreparedStatement pstmt = con.prepareStatement("SELECT * FROM command");
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Command c = new Command();
                c.setCmd((rs.getString("cmd")));
                c.setId(rs.getInt("id"));
                c.setMsg(rs.getString("msg"));
                cs.add(c);
            }

            con.close();
        } catch (Exception e) {
            Logger.logSevere(e, CommandDAO.class);
        }

        return cs;
    }

    public Command selectTesbr(Command c) throws SQLException, ClassNotFoundException {
        try (Connection con = DatabaseConnection.getTesbrConnection()) {
            PreparedStatement pstmt = con.prepareStatement("SELECT * FROM command WHERE comando = ?");
            pstmt.setString(1, c.getCmd());
            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                c.setCmd((rs.getString("cmd")));
                c.setId(rs.getInt("id"));
                c.setMsg(rs.getString("msg"));
            }

            con.close();
        } catch (Exception e) {
            Logger.logSevere(e, CommandDAO.class);
        }
        return c;
    }
}
