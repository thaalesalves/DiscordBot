/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.dao;

import es.thalesalv.discordbotjda.model.Gartic;
import es.thalesalv.discordbotjda.util.DatabaseConnection;
import es.thalesalv.discordbotjda.util.Logger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GarticDAO {

    public void insert(Gartic g) throws SQLException, ClassNotFoundException {
        try (Connection con = DatabaseConnection.getTesbrConnection()) {
            PreparedStatement pstmt = con.prepareStatement("INSERT INTO gartic VALUES(DEFAULT, ?, ?, ?, ?, ?)");
            pstmt.setString(1, g.getTema());
            pstmt.setString(2, g.getUrl());
            pstmt.setString(3, g.getDificuldade());
            pstmt.setString(4, g.getSenha());
            pstmt.setString(5, g.getPontuacao());
            pstmt.executeUpdate();
            con.close();
        } catch (Exception e) {
            Logger.logSevere(e, GarticDAO.class);
        }
    }

    public Gartic select(Gartic g) throws SQLException, ClassNotFoundException {
        try (Connection con = DatabaseConnection.getTesbrConnection()) {
            PreparedStatement pstmt = con.prepareStatement("SELECT * FROM gartic WHERE tema = ?");
            pstmt.setString(1, g.getTema());
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                g.setId(rs.getInt("id"));
                g.setDificuldade(rs.getString("dificuldade"));
                g.setPontuacao(rs.getString("pontuacao"));
                g.setSenha(rs.getString("senha"));
                g.setUrl(rs.getString("url"));
            }

            con.close();
        } catch (Exception e) {
            Logger.logSevere(e, GarticDAO.class);
        }

        return g;
    }

    public List<Gartic> select() throws SQLException, ClassNotFoundException {
        List<Gartic> gs = new ArrayList<Gartic>();

        try (Connection con = DatabaseConnection.getTesbrConnection()) {
            PreparedStatement pstmt = con.prepareStatement("SELECT * FROM gartic");
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Gartic g = new Gartic();
                g.setId(rs.getInt("id"));
                g.setDificuldade(rs.getString("dificuldade"));
                g.setPontuacao(rs.getString("pontuacao"));
                g.setSenha(rs.getString("senha"));
                g.setTema(rs.getString("tema"));
                g.setUrl(rs.getString("url"));
                gs.add(g);
            }

            con.close();
        } catch (Exception e) {
            Logger.logSevere(e, GarticDAO.class);
        }

        return gs;
    }
}
