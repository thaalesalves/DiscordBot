/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.text.WordUtils;

public final class MediaWiki {

    public static boolean isBetween(String begin, String end, String content) {
        try {
            Pattern p = Pattern.compile(begin + "(.*?)" + end);
            Matcher m = p.matcher(content);
            if (m.find()) {
                return true;
            }
        } catch (Exception e) {
            Logger.logWarning(e, MediaWiki.class);
            return false;
        }

        return false;
    }

    public static String getBetweenRedirect(String content) {
        try {
            if (content.contains("REDIRECT[[")) {
                Pattern p = Pattern.compile("REDIRECT\\[\\[Lore:(.*?)\\]\\]");
                Matcher m = p.matcher(content);
                while (m.find()) {
                    return m.group(1);
                }
            }
            if (content.contains("REDIRECT [[")) {
                Pattern p = Pattern.compile("REDIRECT \\[\\[Lore:(.*?)\\]\\]");
                Matcher m = p.matcher(content);
                while (m.find()) {
                    return m.group(1);
                }
            }
        } catch (Exception e) {
            Logger.logSevere(e, MediaWiki.class);
        }

        return content;
    }

    public static String getBetweenDiscord(String content) {
        try {
            if (content.contains("{{Discord|")) {
                Pattern p = Pattern.compile("\\{\\{Discord\\|(.*?)\\}\\}");
                Matcher m = p.matcher(content);
                while (m.find()) {
                    return m.group(1);
                }
            }
        } catch (Exception e) {
            Logger.logSevere(e, MediaWiki.class);
        }

        return content;
    }

    public static String getBetween(String begin, String end, String content) {
        try {
            if (isBetween(begin, end, content)) {
                Pattern p = Pattern.compile(begin + "(.*?)" + end);
                Matcher m = p.matcher(content);
                while (m.find()) {
                    return m.group(1);
                }
            }
        } catch (Exception e) {
            Logger.logSevere(e, MediaWiki.class);
        }

        throw new NullPointerException("Erro ao pegar o valor entre " + begin + " e " + end);
    }

    public static String parsePageTitle(String title) {

        title = WordUtils.capitalize(title);
        return title
                .replace(" ", "_")
                .replace("De", "de")
                .replace("Da", "da")
                .replace("Do", "do")
                .replace("Na", "na")
                .replace("No", "no")
                .replace("Para", "para")
                .replace("deshaan", "Deshaan")
                .replace("vvardenfell", "Vvardenfell")
                .replace("(Deus)", "(deus)")
                .replace("(Cidade)", "(cidade")
                .replace("(City)", "(city)")
                .replace("(God)", "(god)");
    }
}
