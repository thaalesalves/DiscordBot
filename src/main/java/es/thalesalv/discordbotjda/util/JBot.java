/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

@lombok.Getter
public final class JBot {
// <editor-fold defaultstate="collapsed" desc="Inicializador e constantes">

    public static final String HOME;
    public static final String TEST;
    public static final String SOURCE;
    public static final String RESOURCE;
    public static final String LIB;
    public static final String TARGET;
    public static final String WEBAPP;
    public static final String VERSION;
    private static String tokenUesp;
    private static String tokenTesbr;
    private static String operator;
    private static String domain;
    private static String domainHost;
    private static String netbios;
    private static String authMethod;
    private static String authPort;
    private static String dbUser;
    private static String dbName;
    private static String dbPasswd;
    private static String dbAddr;
    private static String dbPort;
    private static String dbSsl;
    private static String dbDbms;

    public static String getOperator() {
        return operator;
    }

    public static String getTokenUesp() {
        return tokenUesp;
    }

    public static String getTokenTesbr() {
        return tokenTesbr;
    }

    static {
        Properties app = new Properties();

        try {
            InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("jbot.properties");
            app.load(stream);
        } catch (Exception e) {
            Logger.logSevere(e, JBot.class);
        }

        VERSION = (String) app.get("jbot.info.version");
        HOME = formatURI((String) app.get("jbot.dir.home"));
        TEST = formatURI((String) app.get("jbot.dir.test"));
        SOURCE = formatURI((String) app.get("jbot.dir.source"));
        RESOURCE = formatURI((String) app.get("jbot.dir.resource"));
        LIB = formatURI((String) app.get("jbot.dir.lib"));
        TARGET = formatURI((String) app.get("jbot.dir.target"));
        WEBAPP = formatURI((String) app.get("jbot.dir.webapp"));

        JBot.loadProperties();
    }      // </editor-fold>

    private JBot() {
        throw new AssertionError();
    }

    private static String formatURI(String uri) {
        return uri.replace("\\", "/");
    }

    public static void loadProperties() {
        try {
            Properties cfg = new Properties();
            InputStream stream = new FileInputStream(TARGET + "/classes/jbot.properties");
            cfg.load(stream);

            tokenUesp = (String) cfg.get("jbot.info.token.uesp");
            tokenTesbr = (String) cfg.get("jbot.info.token.tesbr");
            operator = (String) cfg.get("jbot.info.operator");
        } catch (Exception e) {
            Logger.logSevere(e, JBot.class);
        }
    }

    public static void writeProperty(String key, String value) {
        try {
            Properties configProperty = new Properties();

            File file = new File(TARGET + "/classes/jbot.properties");
            FileInputStream fileIn = new FileInputStream(file);
            configProperty.load(fileIn);
            configProperty.setProperty(key, value);
            FileOutputStream fileOut = new FileOutputStream(file);
            configProperty.store(fileOut, null);

            fileOut.close();

            loadProperties();
        } catch (Exception e) {
            Logger.logSevere(e, JBot.class);
        }
    }
}
