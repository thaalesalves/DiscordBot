/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.util;

import java.sql.Connection;
import java.sql.DriverManager;

public final class DatabaseConnection {
    
    public static Connection getUespConnection() {
        Connection conn = null;

        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(
                    "jdbc:postgresql://localhost/disc_uesp",
                    "discord",
                    "discord"
            );
        } catch (ClassNotFoundException e) {
            Logger.logSevere(e, DatabaseConnection.class);
        } catch (Exception e) {
            Logger.logSevere(e, DatabaseConnection.class);
        }

        return conn;
    }
    
    public static Connection getTesbrConnection() {
        Connection conn = null;

        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(
                    "jdbc:postgresql://localhost/disc_tesbr",
                    "discord",
                    "discord"
            );
        } catch (ClassNotFoundException e) {
            Logger.logSevere(e, DatabaseConnection.class);
        } catch (Exception e) {
            Logger.logSevere(e, DatabaseConnection.class);
        }

        return conn;
    }
}
