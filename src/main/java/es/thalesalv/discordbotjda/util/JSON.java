/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import org.json.JSONObject;

public final class JSON {

    static class Item {

        String title;
        String link;
        String description;
    }

    static class Page {

        String title;
        String link;
        String description;
        String language;
        List<Item> items;
    }

    public static JSONObject parseJson(String json) {
        JSONObject obj = null;
        try {
            obj = new JSONObject(json);
        } catch (Exception e) {
            Logger.logSevere(e, JSON.class);
        }

        return obj;
    }

    public static String beautify(String json) {
        Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();

        return gson.toJson(json);
    }

    public static String toJson(Object obj) {
        Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();

        return gson.toJson(obj);
    }

    public static String toJson(String urlString) throws IOException {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1) {
                buffer.append(chars, 0, read);
            }

            return buffer.toString();
        } catch (Exception e) {
            Logger.logSevere(e, JSON.class);
        } finally {
            if (reader != null) {
                reader.close();
            }
        }

        return null;
    }
}
