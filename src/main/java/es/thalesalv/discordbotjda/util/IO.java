/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.util;

import java.io.PrintStream;
import java.util.Locale;
import java.util.Scanner;
import org.apache.commons.lang3.text.WordUtils;

public class IO {

    static Scanner in = new Scanner(System.in);
    static PrintStream out = new PrintStream(System.out);

    public static String read() {
        return in.next();
    }

    public static String readln() {
        return in.nextLine();
    }

    public static Integer readInt() {
        return in.nextInt();
    }

    public static Float readFloat() {
        return in.nextFloat();
    }

    public static Double readDouble() {
        return in.nextDouble();
    }

    public static void write(String value) {
        out.print(value);
    }

    public static void writeln(String value) {
        out.println(value);
    }

    public static void write(Integer value) {
        out.print(value);
    }

    public static void writeln(Integer value) {
        out.println(value);
    }

    public static void write(Float value) {
        out.print(value);
    }

    public static void writeln(Float value) {
        out.println(value);
    }

    public static void write(Double value) {
        out.print(value);
    }

    public static void writeln(Double value) {
        out.println(value);
    }

    public static void printf(String format, Object... args) {
        out.printf(format, args);
    }

    public static void printf(Locale l, String format, Object... args) {
        out.printf(l, format, args);
    }

    public static String capitalize(String param) {
        return WordUtils.capitalize(param);
    }
}
