/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.util;

import java.util.logging.Level;

public class Logger {

    private final static java.util.logging.Logger LOGGER = java.util.logging.Logger.getAnonymousLogger();

    public static void logSevere(Throwable t, Class c) {
        LOGGER.log(Level.SEVERE, "\n=== ERRO ===\nErro em " + c.toString() + ": " + t.getMessage() + "\nExceção lançada: " + t + "\nLinha: " + t.getStackTrace()[0].getLineNumber() + "\n=== FIM DO ERRO ===", t);
    }

    public static void logWarning(Throwable t, Class c) {
        LOGGER.log(Level.WARNING, "\n=== ERRO ===\nErro em " + c.toString() + ": " + t.getMessage() + "\nExceção lançada: " + t + "\nLinha: " + t.getStackTrace()[0].getLineNumber() + "\n=== FIM DO ERRO ===", t);
    }
}
