/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.thalesalv.discordbotjda.model;

@lombok.Getter
@lombok.Setter
public class Image {
    private Integer id;
    private String cmd;
    private String url;
    private String msg;
}
