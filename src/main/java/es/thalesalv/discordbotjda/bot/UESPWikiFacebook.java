/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.bot;

import es.thalesalv.discordbotjda.util.JBot;
import es.thalesalv.discordbotjda.util.Logger;
import java.awt.Color;
import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

@lombok.Getter
@lombok.Setter
public class UESPWikiFacebook extends ListenerAdapter {

    private static String content;
    private static String imageUrl;
    private static String url;

    public static void main(String[] args) throws LoginException, RateLimitedException {
        try {
            JDA jda = new JDABuilder(AccountType.BOT).setToken(JBot.getTokenUesp()).buildAsync();
            jda.addEventListener(new UESPWikiFacebook());
        } catch (Exception e) {
            Logger.logSevere(e, UESPWikiFacebook.class);
        }
    }

    public static void setPost(HttpServletRequest request, HttpServletResponse response) {
        content = request.getParameter("content");
        url = request.getParameter("url");
        if (request.getParameter("oe") != null) {
            imageUrl = request.getParameter("picture") + "&oe=" + request.getParameter("oe");
        } else {
            imageUrl = request.getParameter("picture");
        }
    }

    @Override
    public void onReady(ReadyEvent event) {
        try {
            MessageChannel channel = event.getJDA().getTextChannelById("353340994936373258");
            EmbedBuilder builder = new EmbedBuilder();
            builder.setAuthor("Unofficial Elder Scrolls Pages em Português", null, "https://i.imgur.com/4S08E2M.png");
            builder.setTitle("Novo post da UESPWiki em Português!");
            builder.setColor(Color.RED);
            builder.setFooter("The UESPWiki. Your source for Elder Scrolls lore since 1995.", "https://i.imgur.com/4S08E2M.png");
            builder.setDescription(content + "\n\nLink do post: " + url);
            builder.setImage(imageUrl);
            channel.sendMessage(builder.build()).complete();
        } catch (Exception e) {
            Logger.logSevere(e, UESPWikiFacebook.class);
            event.getJDA().shutdown();
        }
    }
}
