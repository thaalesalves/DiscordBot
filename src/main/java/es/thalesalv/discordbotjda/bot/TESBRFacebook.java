/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.bot;

import es.thalesalv.discordbotjda.util.JBot;
import es.thalesalv.discordbotjda.util.Logger;
import java.awt.Color;
import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

@lombok.Getter
@lombok.Setter
public class TESBRFacebook extends ListenerAdapter {

    private static String content;
    private static String imageUrl;
    private static String url;

    public static void main(String[] args) throws LoginException, RateLimitedException {
        try {
            JDA jda = new JDABuilder(AccountType.BOT).setToken(JBot.getTokenTesbr()).buildAsync();
            jda.addEventListener(new TESBRFacebook());
        } catch (Exception e) {
            Logger.logSevere(e, TESBRFacebook.class);
        }
    }

    public static void setPost(HttpServletRequest request, HttpServletResponse response) {
        content = request.getParameter("content");
        url = request.getParameter("url");
        if (request.getParameter("oe") != null) {
            imageUrl = request.getParameter("picture") + "&oe=" + request.getParameter("oe");
        } else {
            imageUrl = request.getParameter("picture");
        }
    }

    @Override
    public void onReady(ReadyEvent event) {
        try {
            MessageChannel channel = event.getJDA().getTextChannelById("363745781057847296");
            EmbedBuilder builder = new EmbedBuilder();
            builder.setAuthor("The Elder Scrolls Brasil", null, "https://scontent.fgru6-1.fna.fbcdn.net/v/t1.0-9/20841006_1658564530822527_421065287652179817_n.png?oh=7970b7d4f8ad7c07ed1ba2c3f9521514&oe=5A494BE7");
            builder.setTitle("Novo post da Elder Scrolls Brasil!");
            builder.setColor(Color.BLUE);
            builder.setFooter("The Elder Scrolls Brasil. A maior comunidade de Elder Scrolls do Brasil!",
                    "https://scontent.fgru6-1.fna.fbcdn.net/v/t1.0-9/20841006_1658564530822527_421065287652179817_n.png?oh=7970b7d4f8ad7c07ed1ba2c3f9521514&oe=5A494BE7");
            builder.setDescription(content + "\n\nLink do post: " + url);
            builder.setImage(imageUrl);
            channel.sendMessage(builder.build()).complete();
        } catch (Exception e) {
            Logger.logSevere(e, TESBRFacebook.class);
            event.getJDA().shutdown();
        }
    }
}
