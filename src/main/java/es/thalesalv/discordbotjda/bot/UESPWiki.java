/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.bot;

import es.thalesalv.discordbotjda.cmd.AdminCommand;
import es.thalesalv.discordbotjda.cmd.CommandCommand;
import es.thalesalv.discordbotjda.cmd.ImageCommand;
import es.thalesalv.discordbotjda.cmd.LoreCommand;
import es.thalesalv.discordbotjda.model.Lore;
import es.thalesalv.discordbotjda.util.IO;
import es.thalesalv.discordbotjda.util.JBot;
import es.thalesalv.discordbotjda.util.Logger;
import es.thalesalv.discordbotjda.util.MediaWiki;
import java.awt.Color;
import java.util.Random;
import javax.security.auth.login.LoginException;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.exceptions.InsufficientPermissionException;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class UESPWiki extends ListenerAdapter {

    private Guild guild;
    private MessageChannel channel;
    private User author;
    private Message message;
    private String content;
    private String cmd;
    private String param;
    private String op = "$";

    public static void main(String[] args)
            throws LoginException, RateLimitedException {
        JDA jda = new JDABuilder(AccountType.BOT).setToken(JBot.getTokenUesp()).buildAsync();
        jda.addEventListener(new UESPWiki());
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        Lore l = new Lore();
        EmbedBuilder builder = new EmbedBuilder();
        builder.setColor(Color.RED);
        builder.setFooter("The UESPWiki. Your source for Elder Scrolls lore since 1995.", "https://i.imgur.com/4S08E2M.png");

        try {
            guild = event.getGuild();
            channel = event.getChannel();
            author = event.getAuthor();
            message = event.getMessage();
            content = message.getContent();
            cmd = content.split("\\s+")[0];
            param = content.replace(op + cmd, "").trim();
            ImageCommand ic = new ImageCommand();
            CommandCommand cc = new CommandCommand();

            IO.printf("[%s][%s] %#s: %s%n", guild.getName(), channel.getName(), author, content);

            if (!author.isBot()) {
                if (event.isFromType(ChannelType.TEXT)) {
                    if (cmd.equalsIgnoreCase(op + "lore")) {
                        LoreCommand lc = new LoreCommand();
                        if (!(param = content.replace(cmd + " ", "")).equals(cmd)) {
                            if ((l = lc.lore(param)) != null) {
                                builder.setColor(Color.RED);
                                builder.setTitle(l.getTitle());
                                builder.setDescription(l.getContent() + "\n\nLeia mais em " + l.getUrl().replace(" ", "_"));
                            } else {
                                builder.setTitle("Lore:" + MediaWiki.parsePageTitle(param));
                                builder.setDescription("Opa! Não achei a página Lore:" + MediaWiki.parsePageTitle(param) + "! Será que você digitou o nome certo?");
                            }
                            channel.sendMessage(builder.build()).complete();
                        } else {
                            builder.setTitle("Opa, parece que você esqueceu do parâmetro!");
                            builder.setDescription("Opa! Você usou este comando sem parâmetro! sintaxe: " + op + "lore *<termo>*");
                            builder.setColor(Color.RED);
                            channel.sendMessage(builder.build()).complete();
                        }
                    } else if (cmd.equalsIgnoreCase(op + "ajuda") || cmd.equalsIgnoreCase(op + "help")) {
                        AdminCommand admin = new AdminCommand();
                        channel.sendMessage(admin.helpUesp().build()).complete();
                    } else {
                        if (cc.selecionarComandoUesp(cmd, op) != null) {
                            builder.setTitle(cc.selecionarComandoUesp(cmd, op).getCmd());
                            builder.setDescription(cc.selecionarComandoUesp(cmd, op).getMsg());
                            channel.sendMessage(builder.build()).complete();
                        } else if (ic.selecionarImagemUesp(cmd, op) != null) {
                            builder.setTitle(ic.selecionarImagemUesp(cmd, op).getMsg());
                            builder.setImage(ic.selecionarImagemUesp(cmd, op).getUrl());
                            channel.sendMessage(builder.build()).complete();
                        } else if (cmd.startsWith(op)) {
                            builder.setTitle("Erro!");
                            builder.setDescription("Opa! O comando **" + cmd.replace(op, "") + "** não existe!");
                            channel.sendMessage(builder.build()).complete();
                        }
                    }
                } else {
                    System.out.printf("[PM] %#s: %s%n", author, content);
                }
            }
        } catch (IllegalArgumentException e) {
            articlePrivateMessage(l, guild.getMemberById("191672198233063425").getUser(), builder);
            builder.setTitle(l.getTitle());
            builder.setDescription("Opa! Parece que o artigo " + l.getTitle() + " ainda não está preparado para o Discord. Mas não se preocupe, eu enviei uma notificação para a administração da UESPWiki.\n\nMas calma! Você ainda pode ler o artigo em em http://pt.uesp.net/wiki/" + l.getTitle().replace(" ", "_"));
            channel.sendMessage(builder.build()).complete();
        } catch (Exception e) {
            Logger.logSevere(e, UESPWiki.class);
        }
    }

    public int nextInt(int origin, int bound) {
        Random random = new Random();
        if (origin < bound) {
            int n = bound - origin;
            if (n > 0) {
                return random.nextInt(n) + origin;
            } else {
                int r;
                do {
                    r = random.nextInt();
                } while (r < origin || r >= bound);
                return r;
            }
        } else {
            return random.nextInt();
        }
    }

    public void articlePrivateMessage(Lore l, User user, EmbedBuilder builder) {
        user.openPrivateChannel().queue((channel)
                -> {
            builder.setAuthor(author.getName(), null, author.getAvatarUrl());
            builder.setTitle("Adaptação da página " + l.getTitle() + " para o Discord");
            builder.setDescription("A página " + l.getTitle() + " ainda não foi adaptada para o Discord, e o membro " + author.getName() + " está tentando lê-la.\n\nLink do artigo: http://pt.uesp.net/wiki/" + l.getTitle().replace(" ", "_"));
            channel.sendMessage(builder.build()).complete();
        });
    }

    protected void checkPermission(Permission perm) {
        if (!guild.getSelfMember().hasPermission(perm)) {
            channel.sendMessage("Você não tem permissão suficiente, " + author.getAsMention() + ".").complete();
            throw new InsufficientPermissionException(perm);
        }
    }
}
