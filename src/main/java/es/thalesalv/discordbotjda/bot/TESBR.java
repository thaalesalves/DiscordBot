/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.bot;

import es.thalesalv.discordbotjda.cmd.AdminCommand;
import es.thalesalv.discordbotjda.cmd.GarticCommand;
import es.thalesalv.discordbotjda.cmd.CommandCommand;
import es.thalesalv.discordbotjda.cmd.ImageCommand;
import es.thalesalv.discordbotjda.util.IO;
import es.thalesalv.discordbotjda.util.JBot;
import es.thalesalv.discordbotjda.util.Logger;
import java.awt.Color;
import java.util.List;
import javax.security.auth.login.LoginException;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class TESBR extends ListenerAdapter {

    private Guild guild;
    private MessageChannel channel;
    private User author;
    private Message message;
    private List<User> mentions;
    private String content;
    private String cmd;
    private String param;
    private String op;
    private MessageChannel general;
    private MessageChannel anuncios;

    public static void main(String[] args)
            throws LoginException, RateLimitedException {
        JDA jda = new JDABuilder(AccountType.BOT).setToken(JBot.getTokenTesbr()).buildAsync();
        jda.addEventListener(new TESBR());
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        try {
            op = "/";
            guild = event.getGuild();
            channel = event.getChannel();
            author = event.getAuthor();
            message = event.getMessage();
            mentions = message.getMentionedUsers();
            content = message.getContent();
            cmd = content.split("\\s+")[0];
            param = content.replace(cmd + " ", "");
            general = guild.getTextChannelById("304236194055716864");
            anuncios = guild.getTextChannelById("363745781057847296");
            ImageCommand ic = new ImageCommand();
            CommandCommand cc = new CommandCommand();

            System.out.printf("[%s][%s] %#s: %s%n", guild.getName(), channel.getName(), author, content);

            if (!author.isBot()) {
                if (event.isFromType(ChannelType.TEXT)) {
                    if (cmd.equalsIgnoreCase(op + "ban")) {
                        String motivo = param.replace(param.split("\\s+")[0], "");
                        AdminCommand admin = new AdminCommand();
                        EmbedBuilder builder = new EmbedBuilder();
                        builder.setColor(Color.BLUE);
                        builder.setFooter("The Elder Scrolls Brasil. A maior comunidade de Elder Scrolls do Brasil!",
                                "https://i.imgur.com/vuoeSJP.png");
                        builder = admin.ban(guild.getMemberById(author.getId()), mentions.get(0), guild, motivo);
                        channel.sendMessage(builder.build()).complete();
                    } else if (cmd.equalsIgnoreCase(op + "chute") || cmd.equalsIgnoreCase(op + "kick")) {
                        String motivo = param.replace(param.split("\\s+")[0], "");
                        AdminCommand admin = new AdminCommand();
                        EmbedBuilder builder = new EmbedBuilder();
                        builder.setColor(Color.BLUE);
                        builder.setFooter("The Elder Scrolls Brasil. A maior comunidade de Elder Scrolls do Brasil!",
                                "https://i.imgur.com/vuoeSJP.png");
                        Member member = guild.getMemberById(author.getId());
                        builder = admin.kick(member, mentions.get(0), guild, motivo);
                        channel.sendMessage(builder.build()).complete();
                    } else if (cmd.equalsIgnoreCase(op + "aviso") || cmd.equalsIgnoreCase(op + "warn")) {
                        EmbedBuilder builder = new EmbedBuilder();
                        builder.setColor(Color.BLUE);
                        builder.setFooter("The Elder Scrolls Brasil. A maior comunidade de Elder Scrolls do Brasil!",
                                "https://i.imgur.com/vuoeSJP.png");
                        String motivo = param.replace(param.split("\\s+")[0], "");
                        for (Role i : author.getJDA().getRoles()) {
                            if (i.getName().equals("@adm")) {
                                if (motivo == null || motivo.isEmpty()) {
                                    AdminCommand admin = new AdminCommand();
                                    builder = admin.warn(mentions.get(0), motivo);
                                    general.sendMessage(builder.build()).complete();
                                } else {
                                    builder.setTitle("Erro!");
                                    builder.setDescription("Opa! Você não pode advertir um membro sem informar um motivo");
                                    channel.sendMessage(builder.build()).complete();
                                }
                            }
                        }
                    } else if (cmd.equalsIgnoreCase(op + "ajuda") || cmd.equalsIgnoreCase(op + "help")) {
                        AdminCommand admin = new AdminCommand();
                        channel.sendMessage(admin.helpTesbr(cmd).build()).complete();
                    } else if (cmd.equalsIgnoreCase(op + "gartic")) {
                        GarticCommand gc = new GarticCommand();
                        if (!(param = content.replace(cmd + " ", "")).equals(cmd)) {
                            param = IO.capitalize(param);
                            channel.sendMessage(gc.getGartic(param).build()).complete();
                        } else {
                            channel.sendMessage(gc.getGarticMenu().build()).complete();
                        }
                    } else if (cmd.equalsIgnoreCase(op + "anuncio") || cmd.equalsIgnoreCase(op + "announce")) {
                        for (Role i : author.getJDA().getRoles()) {
                            if (i.getName().equals("@adm")) {
                                AdminCommand admin = new AdminCommand();
                                anuncios.sendMessage(admin.announce(param).build()).complete();
                            }
                        }
                    } else {
                        EmbedBuilder builder = new EmbedBuilder();
                        builder.setColor(Color.BLUE);
                        builder.setFooter("The Elder Scrolls Brasil. A maior comunidade de Elder Scrolls do Brasil!",
                                "https://i.imgur.com/vuoeSJP.png");

                        if (cc.selecionarComandoTesbr(cmd, op) != null) {
                            builder.setTitle(cc.selecionarComandoTesbr(cmd, op).getCmd());
                            builder.setDescription(cc.selecionarComandoTesbr(cmd, op).getMsg());
                            channel.sendMessage(builder.build()).complete();
                        } else if (ic.selecionarImagemTesbr(cmd, op) != null) {
                            builder.setTitle(ic.selecionarImagemTesbr(cmd, op).getMsg());
                            builder.setImage(ic.selecionarImagemTesbr(cmd, op).getUrl());
                            channel.sendMessage(builder.build()).complete();
                        } else if (cmd.startsWith(op)) {
                            builder.setTitle("Erro!");
                            builder.setDescription("Opa! O comando **" + cmd.replace(op, "") + "** não existe!");
                            channel.sendMessage(builder.build()).complete();
                        }
                    }
                } else {
                    System.out.printf("[PM] %#s: %s%n", author, content);
                }
            }
        } catch (Exception e) {
            Logger.logSevere(e, TESBR.class);
        }
    }

    protected boolean checkPermission(Member member, Permission perm) {
        if (!member.hasPermission(perm)) {
            return false;
        }
        return true;
    }
}
