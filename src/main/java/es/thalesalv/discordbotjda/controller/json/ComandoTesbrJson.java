/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.controller.json;

import es.thalesalv.discordbotjda.dao.CommandDAO;
import es.thalesalv.discordbotjda.model.Command;
import es.thalesalv.discordbotjda.util.JSON;
import es.thalesalv.discordbotjda.util.Logger;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ComandoTesbrJson implements controller.json.IJson {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException, NamingException, IOException, NullPointerException {
        try {
            Command c = new Command();
            CommandDAO cdao = new CommandDAO();
            List<Command> cs = cdao.selectTesbr();
            
            return JSON.toJson(cs);
        } catch (Exception e) {
            Logger.logSevere(e, ComandoTesbrJson.class);
        }

        return "";
    }
}
