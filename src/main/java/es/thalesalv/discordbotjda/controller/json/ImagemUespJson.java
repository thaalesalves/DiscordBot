/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.controller.json;

import es.thalesalv.discordbotjda.dao.ImageDAO;
import es.thalesalv.discordbotjda.model.Image;
import es.thalesalv.discordbotjda.util.JSON;
import es.thalesalv.discordbotjda.util.Logger;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ImagemUespJson implements controller.json.IJson {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException, NamingException, IOException, NullPointerException {
        try {            
            Image i = new Image();
            ImageDAO idao = new ImageDAO();
            List<Image> is = idao.selectUesp();
            
            return JSON.toJson(is);
        } catch (Exception e) {
            Logger.logSevere(e, ImagemUespJson.class);
        }

        return "";
    }
}
