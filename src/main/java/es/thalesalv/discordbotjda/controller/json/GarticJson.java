/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.controller.json;

import es.thalesalv.discordbotjda.dao.GarticDAO;
import es.thalesalv.discordbotjda.model.Gartic;
import es.thalesalv.discordbotjda.util.JSON;
import es.thalesalv.discordbotjda.util.Logger;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GarticJson implements controller.json.IJson {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException, NamingException, IOException, NullPointerException {
        try {
            GarticDAO gdao = new GarticDAO();
            List<Gartic> gs = gdao.select();
            
            return JSON.toJson(gs);
        } catch (Exception e) {
            Logger.logSevere(e, GarticJson.class);
        }
        throw new NullPointerException("Algo estava nulo enquanto o objeto JSON era gerado.");
    }
}
