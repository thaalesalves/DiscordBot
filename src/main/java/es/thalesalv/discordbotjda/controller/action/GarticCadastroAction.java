/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.controller.action;

import es.thalesalv.discordbotjda.dao.GarticDAO;
import es.thalesalv.discordbotjda.model.Gartic;
import es.thalesalv.discordbotjda.util.Logger;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ConnectException;
import java.sql.SQLException;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class GarticCadastroAction implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, FileNotFoundException, SQLException, ConnectException, IOException, NamingException, ServletException {
        HttpSession session = request.getSession();
        
        try {
            Gartic g = new Gartic();
            GarticDAO gdao = new GarticDAO();

            g.setDificuldade(request.getParameter("dificuldade"));
            g.setPontuacao(request.getParameter("pontos"));
            g.setSenha(request.getParameter("senha"));
            g.setTema(request.getParameter("tema"));
            g.setUrl(request.getParameter("url"));
            gdao.insert(g);
            session.setAttribute("msg", "Sala de Gartic cadastrada com sucesso");
            session.setAttribute("status", "success");
        } catch (Exception e) {
            Logger.logSevere(e, GarticCadastroAction.class);
            session.setAttribute("msg", "Erro ao cadastrar sala");
            session.setAttribute("status", "error");
            return request.getContextPath() + "/bot/tesbr/gartic.jsp";
        }

        return request.getContextPath() + "/bot/tesbr/gartic.jsp";
    }

}
