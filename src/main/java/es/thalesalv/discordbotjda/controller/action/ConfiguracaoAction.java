/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.controller.action;

import es.thalesalv.discordbotjda.bot.TESBR;
import es.thalesalv.discordbotjda.bot.UESPWiki;
import es.thalesalv.discordbotjda.util.JBot;
import es.thalesalv.discordbotjda.util.Logger;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ConnectException;
import java.sql.SQLException;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ConfiguracaoAction implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, FileNotFoundException, SQLException, ConnectException, IOException, NamingException, ServletException {
        HttpSession session = request.getSession();

        try {
            String token = request.getParameter("token");
            session.setAttribute("msg", "Configurações alteradas com sucesso");
            session.setAttribute("status", "success");
            JBot.writeProperty("jbot.info.operator", "/");

            if (request.getParameter("bot").equals("uesp")) {
                JBot.writeProperty("jbot.info.token.uesp", token);
                UESPWiki.main(null);
                return request.getContextPath() + "/bot/uesp/configs.jsp";
            } else {
                JBot.writeProperty("jbot.info.token.tesbr", token);
                TESBR.main(null);
                return request.getContextPath() + "/bot/tesbr/configs.jsp";
            }
        } catch (Exception e) {
            session.setAttribute("msg", "Erro ao alterar as configurações");
            session.setAttribute("status", "error");
            Logger.logSevere(e, ImagemInsercaoAction.class);
            if (request.getParameter("bot").equals("uesp")) {
                return request.getContextPath() + "/bot/uesp/configs.jsp";
            } else {
                return request.getContextPath() + "/bot/tesbr/configs.jsp";
            }
        }
    }
}
