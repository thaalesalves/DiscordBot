/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.controller.action;

import es.thalesalv.discordbotjda.dao.ImageDAO;
import es.thalesalv.discordbotjda.model.Image;
import es.thalesalv.discordbotjda.util.Logger;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ConnectException;
import java.sql.SQLException;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ImagemInsercaoAction implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, FileNotFoundException, SQLException, ConnectException, IOException, NamingException, ServletException {
        HttpSession session = request.getSession();

        try {
            ImageDAO dao = new ImageDAO();
            Image img = new Image();

            img.setCmd(request.getParameter("comando"));
            img.setUrl(request.getParameter("img"));
            img.setMsg(request.getParameter("msg"));
            String bot = request.getParameter("bot");

            session.setAttribute("msg", "Imagem cadastrada com sucesso");
            session.setAttribute("status", "success");

            if (bot.equals("uesp")) {
                dao.insertUesp(img);
                return request.getContextPath() + "/bot/uesp/imagens.jsp";
            } else {
                dao.insertTesbr(img);
                return request.getContextPath() + "/bot/tesbr/imagens.jsp";
            }
        } catch (Exception e) {
            session.setAttribute("msg", "Erro ao cadastrar imagem");
            session.setAttribute("status", "error");
            Logger.logSevere(e, ImagemInsercaoAction.class);
            if (request.getParameter("bot").equals("uesp")) {
                return request.getContextPath() + "/bot/uesp/imagens.jsp";
            } else {
                return request.getContextPath() + "/bot/tesbr/imagens.jsp";
            }
        }
    }
}
