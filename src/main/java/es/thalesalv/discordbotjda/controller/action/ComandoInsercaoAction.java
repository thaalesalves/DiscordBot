/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.thalesalv.discordbotjda.controller.action;

import es.thalesalv.discordbotjda.dao.CommandDAO;
import es.thalesalv.discordbotjda.model.Command;
import es.thalesalv.discordbotjda.util.Logger;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ConnectException;
import java.sql.SQLException;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ComandoInsercaoAction implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, FileNotFoundException, SQLException, ConnectException, IOException, NamingException, ServletException {
        HttpSession session = request.getSession();

        try {
           CommandDAO dao = new CommandDAO();
            Command c = new Command();

            c.setCmd(request.getParameter("comando"));
            c.setMsg(request.getParameter("msg"));
            String bot = request.getParameter("bot");
            
            session.setAttribute("msg", "Comando cadastrada com sucesso");
            session.setAttribute("status", "success");
            
            if (bot.equals("uesp")) {
                dao.insertUesp(c);
                return request.getContextPath() + "/bot/uesp/comandos.jsp";
            } else {
                dao.insertTesbr(c);
                return request.getContextPath() + "/bot/tesbr/comandos.jsp";
            }
        } catch (Exception e) {
            session.setAttribute("msg", "Erro ao cadastrar comando");
            session.setAttribute("status", "error");
            Logger.logSevere(e, ComandoInsercaoAction.class);
            
            if (request.getParameter("bot").equals("uesp")) {
                return request.getContextPath() + "/bot/uesp/comandos.jsp";
            } else {
                return request.getContextPath() + "/bot/tesbr/comandos.jsp";
            }
        }
    }

}
