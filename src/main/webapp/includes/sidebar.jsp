<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <li class="treeview">
                <a href="${pageContext.request.contextPath}/">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li id="menu-conf" class="treeview">
                <a href="#">
                    <i class="fa fa-facebook"></i> <span>TESBR</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li id="item-admin-ad"><a href="${pageContext.request.contextPath}/bot/tesbr/configs.jsp"><i class="fa fa-circle-o"></i> <span>Configurações</span></a></li>
                    <li id="item-admin-ad"><a href="${pageContext.request.contextPath}/bot/tesbr/imagens.jsp"><i class="fa fa-circle-o"></i> <span>Imagens</span></a></li>
                    <li id="item-admin-ad"><a href="${pageContext.request.contextPath}/bot/tesbr/comandos.jsp"><i class="fa fa-circle-o"></i> <span>Comandos</span></a></li>
                    <li id="item-admin-ad"><a href="${pageContext.request.contextPath}/bot/tesbr/gartic.jsp"><i class="fa fa-circle-o"></i> <span>Gartic</span></a></li>
                </ul>
            </li>
            <li id="menu-conf" class="treeview">
                <a href="#">
                    <i class="fa fa-book"></i> <span>UESPWiki</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li id="item-admin-ad"><a href="${pageContext.request.contextPath}/bot/uesp/configs.jsp"><i class="fa fa-circle-o"></i> <span>Configurações</span></a></li>
                    <li id="item-admin-ad"><a href="${pageContext.request.contextPath}/bot/uesp/imagens.jsp"><i class="fa fa-circle-o"></i> <span>Imagens</span></a></li>
                    <li id="item-admin-ad"><a href="${pageContext.request.contextPath}/bot/uesp/comandos.jsp"><i class="fa fa-circle-o"></i> <span>Comandos</span></a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>