/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var contextPath;

/* UESPWiki */
function carregaComandosUesp() {
    $.ajax({
        url: contextPath + '/JsonController?acao=ComandoUesp',
        type: 'POST',
        cache: false,
        dataType: 'JSON',
        complete: function (e) {
            var obj = JSON.parse(e.responseText);
            var cont = '<table id="tb-uesp" class="table table-bordered table-hover">';
            cont += '<thead><th>Comando</th><th>Mensagem</th>';
            cont += '</tr></thead><tbody>';

            $.each(obj, function (i, item) {
                cont += '<tr class="gradeC">';
                cont += '<td class="center">' + obj[i].cmd + '</td>';
                cont += '<td class="center">' + obj[i].msg + '</td>';
                cont += '</tr>';
            });

            cont += '</tbody></table>';
            cont += '<script>$("#tb-uesp").DataTable();</script>';
            cont += '<script>$(document).ready(function () {$(".preview").hover(function () {$(this).find("img").fadeIn();}, function () {$(this).find("img").fadeOut();});}); </script>';
            $('#tb-div-uesp').append(cont);
        }
    });
}

function carregaImagensUesp() {
    $.ajax({
        url: contextPath + '/JsonController?acao=ImagemUesp',
        type: 'POST',
        cache: false,
        dataType: 'JSON',
        complete: function (e) {
            var obj = JSON.parse(e.responseText);
            var cont = '<table id="tb-uesp" class="table table-bordered table-hover">';
            cont += '<thead><th>Comando</th><th>Mensagem</th><th style="width: 10%;">Imagem</th>';
            cont += '</tr></thead><tbody>';

            $.each(obj, function (i, item) {
                cont += '<tr class="gradeC">';
                cont += '<td class="center">' + obj[i].cmd + '</td>';
                cont += '<td class="center">' + obj[i].msg + '</td>';
                cont += '<td class="center"><img src="' + obj[i].url + '" style="max-width: 100px;"></td>';
                cont += '</tr>';
            });

            cont += '</tbody></table>';
            cont += '<script>$("#tb-uesp").DataTable();</script>';
            cont += '<script>$(document).ready(function () {$(".preview").hover(function () {$(this).find("img").fadeIn();}, function () {$(this).find("img").fadeOut();});}); </script>';
            $('#tb-div-uesp').append(cont);
        }
    });
}

/* Elder Scrolls Brasil */
function carregaComandosTesbr() {
    $.ajax({
        url: contextPath + '/JsonController?acao=ComandoTesbr',
        type: 'POST',
        cache: false,
        dataType: 'JSON',
        complete: function (e) {
            var obj = JSON.parse(e.responseText);
            var cont = '<table id="tb-tesbr" class="table table-bordered table-hover">';
            cont += '<thead><th>Comando</th><th>Mensagem</th>';
            cont += '</tr></thead><tbody>';

            $.each(obj, function (i, item) {
                cont += '<tr class="gradeC">';
                cont += '<td class="center">' + obj[i].cmd + '</td>';
                cont += '<td class="center">' + obj[i].msg + '</td>';
                cont += '</tr>';
            });

            cont += '</tbody></table>';
            cont += '<script>$("#tb-tesbr").DataTable();</script>';
            cont += '<script>$(document).ready(function () {$(".preview").hover(function () {$(this).find("img").fadeIn();}, function () {$(this).find("img").fadeOut();});}); </script>';
            $('#tb-div-tesbr').append(cont);
        }
    });
}

function carregaGartic() {
    $.ajax({
        url: contextPath + '/JsonController?acao=Gartic',
        type: 'POST',
        cache: false,
        dataType: 'JSON',
        complete: function (e) {
            var obj = JSON.parse(e.responseText);
            var cont = '<table id="tb-tesbr" class="table table-bordered table-hover">';
            cont += '<thead><th>Tema</th><th>Dificuldade</th>';
            cont += '<th>Senha</th><th>Pontuação Máxima</th><th>Link</th>';
            cont += '</tr></thead><tbody>';

            $.each(obj, function (i, item) {
                cont += '<tr class="gradeC">';
                cont += '<td class="center">' + obj[i].tema + '</td>';
                cont += '<td class="center">' + obj[i].dificuldade + '</td>';
                cont += '<td class="center">' + obj[i].senha + '</td>';
                cont += '<td class="center">' + obj[i].pontuacao + '</td>';
                cont += '<td class="center"><a href="' + obj[i].url + '">Acessar sala</a></td>';
                cont += '</tr>';
            });

            cont += '</tbody></table>';
            cont += '<script>$("#tb-tesbr").DataTable();</script>';
            cont += '<script>$(document).ready(function () {$(".preview").hover(function () {$(this).find("img").fadeIn();}, function () {$(this).find("img").fadeOut();});}); </script>';
            $('#tb-div-tesbr').append(cont);
        }
    });
}

function carregaImagensTesbr() {
    $.ajax({
        url: contextPath + '/JsonController?acao=ImagemTesbr',
        type: 'POST',
        cache: false,
        dataType: 'JSON',
        complete: function (e) {
            var obj = JSON.parse(e.responseText);
            var cont = '<table id="tb-tesbr" class="table table-bordered table-hover">';
            cont += '<thead><th>Comando</th><th>Mensagem</th><th style="width: 10%;">Imagem</th>';
            cont += '</tr></thead><tbody>';

            $.each(obj, function (i, item) {
                cont += '<tr class="gradeC">';
                cont += '<td class="center">' + obj[i].cmd + '</td>';
                cont += '<td class="center">' + obj[i].msg + '</td>';
                cont += '<td class="center"><img src="' + obj[i].url + '" style="max-width: 100px;"></td>';
                cont += '</tr>';
            });

            cont += '</tbody></table>';
            cont += '<script>$("#tb-tesbr").DataTable();</script>';
            cont += '<script>$(document).ready(function () {$(".preview").hover(function () {$(this).find("img").fadeIn();}, function () {$(this).find("img").fadeOut();});}); </script>';
            $('#tb-div-tesbr').append(cont);
        }
    });
}