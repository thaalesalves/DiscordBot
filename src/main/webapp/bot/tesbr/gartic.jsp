<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Calendar"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Gartic  | JBot</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="icon" type="image/png" sizes="32x32" href="${pageContext.request.contextPath}/img/icon.png">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/iCheck/flat/blue.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/morris/morris.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/datepicker/datepicker3.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/daterangepicker/daterangepicker.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/select2/select2.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/iCheck/all.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/daterangepicker/daterangepicker.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/datepicker/datepicker3.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/iCheck/all.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/colorpicker/bootstrap-colorpicker.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/timepicker/bootstrap-timepicker.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/select2/select2.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/dist/css/skins/_all-skins.min.css">
        <link href="${pageContext.request.contextPath}/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="${pageContext.request.contextPath}/css/pnotify.custom.css" rel="stylesheet" type="text/css"/>
        <link href="${pageContext.request.contextPath}/css/animate.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/datatables/dataTables.bootstrap.css">
        <script src="${pageContext.request.contextPath}/plugins/jQuery/jquery-2.2.3.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/notification.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/pnotify.custom.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/bot/js/imagem.js" type="text/javascript"></script>

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <%
            String msg = "null";
            String status = null;
            if ((msg = (String) session.getAttribute("msg")) != null) {
                msg = (String) session.getAttribute("msg");
                status = (String) session.getAttribute("status");
                session.removeAttribute("msg");
                session.removeAttribute("status");
            }
        %>

        <script>
            $(document).ready(function () {
                notify("<%=msg%>", "<%=status%>");
                carregaGartic();
            });
        </script>  
        <script src="${pageContext.request.contextPath}/js/menus.js" type="text/javascript"></script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
        <div class="wrapper">
            <%@include file="/includes/header.jsp"%>
            <%@include file="/includes/sidebar.jsp"%>
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Gartic
                    </h1>
                </section>
                <!-- Cadastro -->
                <section class="content">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Gartic</h3>
                        </div>
                        <form action="${pageContext.request.contextPath}/WebController" method="post">
                            <div class="box-body">                                 
                                <div class='form-group' style="width:48%; display:inline-block;">
                                    <label>Tema</label>
                                    <input autocomplete="off" name="tema" id="tema" type='text' class='form-control pull-right' placeholder="Morrowind"/>
                                </div>                           
                                <div class="form-group" style="width:48%; display:inline-block; float:right;">
                                    <label>URL</label>
                                    <input name="url" id="url" type='text' class='form-control pull-right' placeholder="http://gartic.com.br/sala" />
                                </div>                             
                                <div class="form-group" style="width:48%; display:inline-block;">
                                    <label>Dificuldade</label>
                                    <input autocomplete="off" name="dificuldade" id="dificuldade" type='text' class='form-control pull-right' placeholder="Impossível"/>
                                </div>                               
                                <div class="form-group" style="width:48%; display:inline-block; float:right;">
                                    <label>Senha</label>
                                    <input name="senha" id="senha" class='form-control pull-right' type='text' placeholder="123456" />
                                </div>
                                <div class="form-group" style="width:48%; display:inline-block;">
                                    <label>Pontuação</label>
                                    <input name="pontos" id="pontos" class='form-control pull-right' type='number' placeholder="200" />
                                </div>
                            </div>
                            <div class="box-footer">
                                <button value="GarticCadastro" name="acao" type="submit" class="btn btn-info pull-right">Enviar</button>
                            </div>
                        </form>
                    </div>
                </section>

                <!-- Tabela da TESBR -->
                <section class="content">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Listagem de Salas</h3>                            
                        </div>
                        <div id="tb-div-tesbr" class="box-body"></div>
                    </div>
                </section>
                <%@include file="/includes/footer.jsp" %>
                <div class="control-sidebar-bg"></div>
            </div>

            <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script> -->
            <script src="${pageContext.request.contextPath}/plugins/daterangepicker/moment.min.js"></script>
            <script src="${pageContext.request.contextPath}/plugins/jQuery/jquery-2.2.3.min.js"></script>
            <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>            
            <script src="${pageContext.request.contextPath}/plugins/datatables/jquery.dataTables.js"></script>
            <script src="${pageContext.request.contextPath}/plugins/datatables/dataTables.bootstrap.min.js"></script>
            <script src="${pageContext.request.contextPath}/plugins/select2/select2.full.min.js"></script>
            <script src="${pageContext.request.contextPath}/plugins/input-mask/jquery.inputmask.js"></script>
            <script src="${pageContext.request.contextPath}/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
            <script src="${pageContext.request.contextPath}/plugins/input-mask/jquery.inputmask.extensions.js"></script>
            <script src="${pageContext.request.contextPath}/plugins/daterangepicker/daterangepicker.js"></script>
            <script src="${pageContext.request.contextPath}/plugins/datepicker/bootstrap-datepicker.js"></script>
            <script src="${pageContext.request.contextPath}/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
            <script src="${pageContext.request.contextPath}/plugins/timepicker/bootstrap-timepicker.min.js"></script>
            <script src="${pageContext.request.contextPath}/plugins/slimScroll/jquery.slimscroll.min.js"></script>
            <script src="${pageContext.request.contextPath}/plugins/iCheck/icheck.min.js"></script>
            <script src="${pageContext.request.contextPath}/plugins/fastclick/fastclick.js"></script>
            <script src="${pageContext.request.contextPath}/dist/js/app.min.js"></script>
            <script src="${pageContext.request.contextPath}/dist/js/demo.js"></script>
    </body>
</html>
