/* 
 * Copyright (C) 2017 thaal
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var contextPath;

function carregaCurso() {
    $.ajax({
        url: contextPath + '/JsonController?acao=Imagem',
        type: 'POST',
        cache: false,
        dataType: 'JSON',
        complete: function (e) {
            var obj = JSON.parse(e.responseText);
            var cont = '<table id="tb-curso" class="table table-bordered table-hover">';
            cont += '<thead><th>Comando</th><th>Mensagem</th><th style="width: 10%;">Imagem</th>';
            cont += '</tr></thead><tbody>';

            $.each(obj, function (i, item) {
                cont += '<tr class="gradeC">';
                cont += '<td class="center">' + obj[i].comando + '</td>';
                cont += '<td class="center">' + obj[i].mensagem + '</td>';
                cont += '<td class="center"><img src="' + obj[i].link + '" style="max-width: 100px;"></td>';
                cont += '</tr>';
            });

            cont += '</tbody></table>';
            cont += '<script>$("#tb-curso").DataTable();</script>';
            cont += '<script>$(document).ready(function () {$(".preview").hover(function () {$(this).find("img").fadeIn();}, function () {$(this).find("img").fadeOut();});}); </script>;'
            $('#tb-div').append(cont);
        }
    });
}